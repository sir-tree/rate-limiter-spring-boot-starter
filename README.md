# Rate-Limiter-Spring-Boot-Starter

#### 介绍
基于springboot（拦截器） + redis（执行lua脚本）实现注解限流，支持的限流算法有：令牌桶，漏桶，滑动窗口和固定窗口算法。

**@BucketLimier**：默认使用令牌桶算法限流

**@WindowLimiter**：默认使用滑动窗口算法限流


#### 使用
1. 导入依赖
```
<dependency>
    <groupId>io.gitee.sir-tree</groupId>
    <artifactId>rate-limiter-spring-boot-starter</artifactId>
    <version>0.0.3</version>
</dependency>
```
2. 使用注解
```
@RequestMapping("/test")
@BucketLimiter
public String test() {
    return "test";
}
```
当注解在方法上时，作用的仅是该方法；
当注解在类上时，作用的是类中的所有方法；
注解配置可传递，即当方法上注解有些参数没有配置时，会使用类上注解的参数。

#### 扩展
1. **支持自定义限流算法**：通过实现Arithmetic类，并在注解中修改arithmetic的值为自定义算法类；
2. **支持自定义Key**：默认提供了Ip和路径的Key实现类，自定义可通过实现Key类，并在注解中修改key的值为自定义Key类；
3. **支持默认参数全局修改**：在application配置文件中修改，rate.limiter.xxx.xxx的方式；
4. **支持自定义注解限流**：只需要在自定义注解中添加@HolderImpl注解，并指定value，就可使用。

#### 将来
支持同类型限流多规则，比如同时应用滑动窗口限流，10秒内10次，100秒内50次这两个规则
