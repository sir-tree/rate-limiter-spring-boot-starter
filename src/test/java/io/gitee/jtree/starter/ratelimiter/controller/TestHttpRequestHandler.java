package io.gitee.jtree.starter.ratelimiter.controller;

import io.gitee.jtree.starter.ratelimiter.annotation.impl.BucketLimiter;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.temporal.ChronoUnit;

@Component("/HttpRequestHandler")
@BucketLimiter(capacity = 1, duration = 10, timeUnit = ChronoUnit.SECONDS)
public class TestHttpRequestHandler implements HttpRequestHandler {

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write("Ces");
        response.getWriter().close();
    }
}
