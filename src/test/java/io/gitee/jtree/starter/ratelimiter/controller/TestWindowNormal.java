package io.gitee.jtree.starter.ratelimiter.controller;

import io.gitee.jtree.starter.ratelimiter.annotation.impl.WindowLimiter;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/window")
public class TestWindowNormal {

    @RequestMapping("/t1")
    @WindowLimiter(limit = 10, window = 10, timeUnit = ChronoUnit.SECONDS)
    public String t1() {
        return "t1";
    }


    @RequestMapping("/t2")
    @WindowLimiter(limit = 10, window = 10, timeUnit = ChronoUnit.SECONDS, type = WindowLimitHolder.Type.FIXED)
    public String t2() {
        return "t2";
    }


}
