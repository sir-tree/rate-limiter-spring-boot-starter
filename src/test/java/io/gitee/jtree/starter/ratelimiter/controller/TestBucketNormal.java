package io.gitee.jtree.starter.ratelimiter.controller;

import io.gitee.jtree.starter.ratelimiter.annotation.impl.BucketLimiter;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl.LeakyBucketArithmetic;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/bucket")
@BucketLimiter(capacity = 5, duration = 10, timeUnit = ChronoUnit.SECONDS)
public class TestBucketNormal {

    @RequestMapping("/t1")
    public String test() {
        return "t1";
    }

    @RequestMapping("/t2")
    @BucketLimiter(body = "测试拉", capacity = -2)
    public String test1() {
        return "t2";
    }

    @RequestMapping("/t3")
    @BucketLimiter(body = "测试3", blackDuration = 30, timeUnit = ChronoUnit.SECONDS)
    public String test3() {
        return "t3";
    }

    @RequestMapping("/t4")
    @BucketLimiter(arithmetic = LeakyBucketArithmetic.class, capacity = 5, fill = 2, duration = 10
            , timeUnit = ChronoUnit.SECONDS, body = "超过等待队列")
    public String test4() {
        return "t4";
    }

}
