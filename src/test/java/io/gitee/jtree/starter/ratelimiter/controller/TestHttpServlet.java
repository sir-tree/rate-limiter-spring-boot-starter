package io.gitee.jtree.starter.ratelimiter.controller;

import io.gitee.jtree.starter.ratelimiter.annotation.impl.BucketLimiter;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.temporal.ChronoUnit;

@Component("/HttpServlet")
@BucketLimiter(capacity = 1, duration = 10, timeUnit = ChronoUnit.SECONDS)
public class TestHttpServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        super.service(req, response);
        response.getWriter().write("Ces");
        response.getWriter().close();
    }

}
