package io.gitee.jtree.starter.ratelimiter.controller;

import io.gitee.jtree.starter.ratelimiter.annotation.impl.BucketLimiter;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.temporal.ChronoUnit;

@Component("/Controller")
@BucketLimiter(capacity = 1, duration = 10, timeUnit = ChronoUnit.SECONDS)
public class TestController implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse response) throws Exception {
        response.getWriter().write("Ces");
        response.getWriter().close();
        return null;
    }
}
