package io.gitee.jtree.starter.ratelimiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

//@SpringBootTest(classes = RateLimiterAutoConfiguration.class)
@SpringBootApplication
@Import(RateLimiterAutoConfiguration.class)
class ApplicationTests {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationTests.class);
    }
}
