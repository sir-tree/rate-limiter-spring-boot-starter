-- 参数初始化
local key = KEYS[1]
local capacity = tonumber(ARGV[1])
local pass = tonumber(ARGV[2])
local duration = tonumber(ARGV[3])
local fair = tonumber(ARGV[4])
local queueId = tonumber(ARGV[5])
local exceedQueueId = -(capacity + 1)
local legacyPassKey = key..'-legacyPass'
local queueKey = key..'-queue'

-- redis初始化
local qMembers = redis.call('lRange', queueKey, 0, -1)
local lpValue = redis.call('get', legacyPassKey)
local lpExpire = redis.call('pTtl', legacyPassKey)

local function min(q)
    if #q == 0 then
        return 0
    end
    local mv = tonumber(q[1])
    local tmp = mv
    for i = 2, #q do
        tmp = tonumber(q[i])
        if tmp < mv then
            mv = tmp
        end
    end
    return mv
end

local function addQueue(qId)
    if qId ~= exceedQueueId then
        return qId
    end
    if #qMembers == capacity then
        return exceedQueueId
    end
    qId = min(qMembers) - 1
    redis.call('rPush', queueKey, qId)
    table.insert(qMembers, tostring(qId))
    return qId
end

-- 1 删除成功，0删除失败
local function delQueue()
    -- >= 队列不为空，队列有元素
    if #qMembers > 0 then
        -- 公平
        if fair > 0 then
            -- 第一个不是当前请求
            if tonumber(qMembers[1]) ~= queueId then
                return 0
            else
                redis.call('lPop', queueKey)
                table.remove(qMembers, 1)
            end
        else -- 不公平
            redis.call('lRem', queueKey, 1, queueId)
            for i = #qMembers, 1, -1 do
                if tostring(qMembers[i]) == queueId then
                    table.remove(qMembers, i)
                end
            end
        end
    end
    -- 队列无元素，或队列第一个是
    return 1
end

-- 第一次/到刷新时间-刷新pass
if lpExpire <= 0 then
    local update = delQueue()
    if update == 1 then
        pass = pass - 1
    else
        queueId = addQueue(queueId)
    end
    redis.call('set', legacyPassKey, pass, 'PX', duration)

    if update == 1 then
        return pass
    end
    return queueId
end

lpValue = tonumber(lpValue)
-- 如果lpValue >= 0 表示当前不需要等待
if lpValue - 1 >= 0 then
    if delQueue() == 0 then
        return queueId
    end
    lpValue = lpValue - 1
    redis.call('set', legacyPassKey, lpValue, 'PX', lpExpire)
    return lpValue
else
    return addQueue(queueId)
end