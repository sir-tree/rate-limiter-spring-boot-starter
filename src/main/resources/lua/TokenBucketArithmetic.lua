-- 参数初始化
local key = KEYS[1]
local maxCapacity = tonumber(ARGV[1])
local fill = tonumber(ARGV[2])
local now = tonumber(ARGV[3])
local duration = tonumber(ARGV[4])
local blackKey = key..'-black'
local blackDuration = tonumber(ARGV[5])
local expire = math.ceil(now / fill) * duration

-- 黑名单
local blackValue = redis.call('get', blackKey)
if blackValue then
    return -1;
end

-- 当前值
local kValue = redis.call('get', key)

-- 第一次
if not kValue then
    -- 当前消耗#刷新时间#容量
    kValue = table.concat({'0', tostring(now + duration), tostring(maxCapacity)}, '#')
end

-- 解析
local parts = {}
for part in string.gmatch(kValue, "[^#]+") do
    table.insert(parts, part)
end

local value = tonumber(parts[1])
local refresh = tonumber(parts[2])
local capacity = tonumber(parts[3])
local fillTimes = math.floor((now - refresh) / duration) + 1

-- 刷新桶令牌
if now >= refresh then
    value = 0
    -- 补充
    capacity = capacity + fill * fillTimes
    -- 最多补满
    if capacity > maxCapacity then
        capacity = maxCapacity
    end
    -- 刷新时间
    refresh = now + duration
end

-- 拿完
if value == capacity then
    -- 加入黑名单
    if blackDuration > 0 then
        redis.call('set', blackKey, '_', 'PX', blackDuration)
        redis.call('del', key)
    end
    return -1
end

-- 期间
value = value + 1
kValue = table.concat({tostring(value), tostring(refresh), tostring(capacity)}, '#')
redis.call('set', key, kValue, 'PX', expire)

return capacity - value