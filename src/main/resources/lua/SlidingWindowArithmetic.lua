local key = KEYS[1]
local limit = tonumber(ARGV[1])
local window = tonumber(ARGV[2])
local now = tonumber(ARGV[3])

local start = now - window
redis.call('zRemRangeByScore', key, 0, start) -- 移除上一个窗口期之前的数据
local size = redis.call('zCard', key)

if size == limit then
    return 0 -- 限制
else
    redis.call('zAdd', key, now, now)
    redis.call('pExpire', key, window + 1000) -- 窗口期 + 1秒后过期
    return 1 -- 通过
end