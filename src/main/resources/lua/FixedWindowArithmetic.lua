local key = KEYS[1]
local limit = tonumber(ARGV[1])
local window = tonumber(ARGV[2])

local expire = redis.call('pTtl', key)
local kValue = redis.call('get', key)

if expire <= 0 then
    redis.call('set', key, 1, 'PX', window)
    return 1 -- 通过
end

kValue = tonumber(kValue)
if kValue == limit then
    return 0 -- 限流
else
    redis.call('set', key, kValue + 1, 'PX', expire)
    return 1 -- 通过
end
