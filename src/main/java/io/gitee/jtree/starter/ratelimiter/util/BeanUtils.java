package io.gitee.jtree.starter.ratelimiter.util;

import io.gitee.jtree.starter.ratelimiter.config.properties.BucketLimitProperties;
import io.gitee.jtree.starter.ratelimiter.config.properties.RateLimitProperties;
import io.gitee.jtree.starter.ratelimiter.config.properties.WindowLimitProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Component
public class BeanUtils {

    public static RedisTemplate<String, String> redisTemplate;
    public static RedisScript<Long> tokenBucketScript;
    public static RedisScript<Long> leakyBucketScript;
    public static RedisScript<Long> slidingWindowScript;
    public static RedisScript<Long> fixedWindowScript;
    public static BucketLimitProperties bucketLimitProperties;
    public static WindowLimitProperties windowLimitProperties;
    public static RateLimitProperties rateLimitProperties;
    @Value("classpath:lua/TokenBucketArithmetic.lua")
    private Resource tokenBucketLuaResource;
    @Value("classpath:lua/LeakyBucketArithmetic.lua")
    private Resource leakyBucketLuaResource;
    @Value("classpath:lua/SlidingWindowArithmetic.lua")
    private Resource slidingWindowLuaResource;
    @Value("classpath:lua/FixedWindowArithmetic.lua")
    private Resource fixedWindowLuaResource;

    @PostConstruct
    public void init() throws Exception {
        tokenBucketScript = new DefaultRedisScript<>(readScript(tokenBucketLuaResource), Long.class);
        leakyBucketScript = new DefaultRedisScript<>(readScript(leakyBucketLuaResource), Long.class);
        slidingWindowScript = new DefaultRedisScript<>(readScript(slidingWindowLuaResource), Long.class);
        fixedWindowScript = new DefaultRedisScript<>(readScript(fixedWindowLuaResource), Long.class);
    }

    private String readScript(Resource resource) throws Exception {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\r\n");
            }
        }
        return sb.toString();
    }

    @Autowired
    public void setRedisTemplate(RedisTemplate<String, String> redisTemplate) {
        BeanUtils.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setBucketLimitProperties(BucketLimitProperties bucketLimitProperties) {
        BeanUtils.bucketLimitProperties = bucketLimitProperties;
    }

    @Autowired
    public void setWindowLimitProperties(WindowLimitProperties windowLimitProperties) {
        BeanUtils.windowLimitProperties = windowLimitProperties;
    }

    @Autowired
    public void setRateLimitProperties(RateLimitProperties rateLimitProperties) {
        BeanUtils.rateLimitProperties = rateLimitProperties;
    }
}
