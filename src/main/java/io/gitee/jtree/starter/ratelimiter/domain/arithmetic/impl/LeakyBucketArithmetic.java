package io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.util.BeanUtils;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 漏桶算法
 */
public class LeakyBucketArithmetic implements Arithmetic {

    @Override
    public boolean tryAcquire(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        BucketLimitHolder bucketLimitHolder = (BucketLimitHolder) holder;

        String pool = bucketLimitHolder.getPool();
        String key = bucketLimitHolder.getKey().getKey(request, response, bucketLimitHolder);

        String capacity = String.valueOf(bucketLimitHolder.getCapacity());
        String pass = String.valueOf(bucketLimitHolder.getLoopholes());
        String duration = String.valueOf(bucketLimitHolder.getTimeUnit().getDuration().multipliedBy(bucketLimitHolder.getDuration()).toMillis());
        String fair = String.valueOf(bucketLimitHolder.getFair().getValue());
        String queueId = String.valueOf(-(bucketLimitHolder.getCapacity() + 1));

        List<String> keys = Collections.singletonList(String.format("%s-%s", pool, key));
        Object[] args = new Object[]{capacity, pass, duration, fair, queueId};
        Long result, exceedQueueId = -(bucketLimitHolder.getCapacity() + 1);
        while (true) {
            result = BeanUtils.redisTemplate.execute(BeanUtils.leakyBucketScript, keys, args);
            if (result == null || result.equals(exceedQueueId)) {
                return false;
            }
            if (result >= 0) {
                return true;
            }
            //等待
            LockSupport.parkNanos(this, TimeUnit.MILLISECONDS.toNanos(10));
            args[args.length - 1] = result.toString();
        }
    }

}
