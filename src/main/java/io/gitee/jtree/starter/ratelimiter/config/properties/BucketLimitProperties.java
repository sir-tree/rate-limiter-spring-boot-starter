package io.gitee.jtree.starter.ratelimiter.config.properties;

import io.gitee.jtree.starter.ratelimiter.config.validation.*;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl.TokenBucketArithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import io.gitee.jtree.starter.ratelimiter.domain.key.impl.IpKey;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;

/**
 * 桶限流算法全局配置
 */
@Data
@Component
@Validated
@ConfigurationProperties(prefix = "rate.limiter.bucket")
public class BucketLimitProperties {

    /**
     * Key存储的池子，用以区分不同池子的Key
     */
    @NotBlank
    private String pool = "BucketLimiterPool";

    /**
     * 值唯一，标识限制上下文, 默认值IpKey，即通过Ip进行限制
     */
    @NotEqualsClass(Key.class)
    private Class<? extends Key> key = IpKey.class;

    /**
     * 桶的容量，默认值30
     */
    @Positive
    private long capacity = 30;

    /**
     * 间隔duration后填充的数量，漏桶算法无效，小于等于0表示和init一样
     */
    private long fill = -1;

    /**
     * 表示duration期间放行的数量，令牌桶算法无效
     */
    @Positive
    private long loopholes = 1;

    /**
     * 间隔时长进行填充，漏桶算法无效 默认1000
     */
    @Positive
    private long duration = 1000;

    /**
     * 从黑名单移除时间，漏桶算法无效 小于等于0表示不开启
     */
    private long blackDuration = -1;

    /**
     * blackDuration和duration时间单位，默认毫秒
     */
    @NotEqualsChronoUnit(ChronoUnit.FOREVER)
    private ChronoUnit timeUnit = ChronoUnit.MILLIS;

    /**
     * 限流算法，默认令牌桶
     */
    @NotEqualsClass(Arithmetic.class)
    private Class<? extends Arithmetic> arithmetic = TokenBucketArithmetic.class;

    /**
     * 漏桶算法是否公平等待，默认公平
     * FALSE 不公平， TRUE公平
     */
    @NotDefaultFair
    private BucketLimitHolder.Fair fair = BucketLimitHolder.Fair.TRUE;

    /**
     * 限流生效响应状态码，默认400
     */
    @Positive
    private int code = 400;

    /**
     * 限流生效响应内容，默认为"Bad Request"
     */
    private String body = "Bad Request";

    /**
     * 限流生效响应内容结构，默认为text/plain
     */
    @MediaTypeValid
    private String mediaType = MediaType.TEXT_PLAIN.toString();

    /**
     * 限流生效响应内容编码，默认为UTF-8
     */
    @CharsetValid
    private String charset = StandardCharsets.UTF_8.name();

}
