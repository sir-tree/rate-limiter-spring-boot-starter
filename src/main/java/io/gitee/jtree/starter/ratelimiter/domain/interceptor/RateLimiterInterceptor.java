package io.gitee.jtree.starter.ratelimiter.domain.interceptor;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 限流拦截器
 */
@Component
public class RateLimiterInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Holder holder = Holder.get(handler);
        if (holder != null) {
            boolean pass = holder.tryAcquire(request, response, holder);
            if (pass) {
                return true;
            }
            response.setStatus(holder.getCode());
            response.setCharacterEncoding(holder.getCharset().name());
            response.setContentType(holder.getMediaType().toString());
            if (StringUtils.hasText(holder.getBody())) {
                PrintWriter writer = response.getWriter();
                writer.write(holder.getBody());
                writer.flush();
            }
            return false;
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

}
