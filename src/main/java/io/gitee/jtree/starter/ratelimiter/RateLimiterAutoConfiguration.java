package io.gitee.jtree.starter.ratelimiter;

import io.gitee.jtree.starter.ratelimiter.config.properties.BucketLimitProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnWebApplication
@EnableConfigurationProperties(BucketLimitProperties.class)
@ComponentScan(basePackages = "io.gitee.jtree.starter.ratelimiter")
public class RateLimiterAutoConfiguration {

}
