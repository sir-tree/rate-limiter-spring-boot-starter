package io.gitee.jtree.starter.ratelimiter.domain.key.impl;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import org.springframework.lang.NonNull;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 根据Ip地址限制
 */
public class IpKey implements Key {

    @Override
    public String getKey(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        return get(request);
    }

    @NonNull
    public static String get(@NonNull HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddr();
        }
        return ip == null || ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
    }
}
