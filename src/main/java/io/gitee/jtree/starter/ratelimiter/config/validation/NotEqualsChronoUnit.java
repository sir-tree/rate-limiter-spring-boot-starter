package io.gitee.jtree.starter.ratelimiter.config.validation;

import io.gitee.jtree.starter.ratelimiter.config.validation.impl.NotEqualsChronoUnitImpl;
import io.gitee.jtree.starter.ratelimiter.config.validation.impl.NotEqualsClassImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEqualsChronoUnitImpl.class)
public @interface NotEqualsChronoUnit {
    String message() default "不能为此值";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    ChronoUnit value();
}
