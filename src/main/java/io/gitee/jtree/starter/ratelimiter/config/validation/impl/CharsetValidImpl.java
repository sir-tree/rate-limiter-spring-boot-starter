package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.CharsetValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.nio.charset.Charset;

public class CharsetValidImpl implements ConstraintValidator<CharsetValid, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            Charset.forName(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
