package io.gitee.jtree.starter.ratelimiter.domain.holder;

import io.gitee.jtree.starter.ratelimiter.annotation.HolderImpl;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 限流上下文基类
 */
@Slf4j
public abstract class Holder {

    @Data
    private static class AnnotationHolder {
        private final Annotation methodAnnotation;
        private final Annotation classAnnotation;

        @Override
        public boolean equals(Object o) {
            if (o instanceof AnnotationHolder) {
                AnnotationHolder oa = (AnnotationHolder) o;
                return Objects.equals(methodAnnotation, oa.methodAnnotation) && Objects.equals(classAnnotation, oa.classAnnotation);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return methodAnnotation != null? methodAnnotation.hashCode(): classAnnotation.hashCode();
        }
    }

    private static final Map<AnnotationHolder, Holder> cache = new ConcurrentHashMap<>();

    public Holder(@Nullable HolderImpl methodAnnotation, @Nullable HolderImpl classAnnotation) {
        if (methodAnnotation == null && classAnnotation == null) {
            throw new NullPointerException("methodAnnotation and classAnnotation can't both null");
        }
    }

    /**
     * 获取Holder实例
     * @param handler 处理器
     * @return 处理器上使用的holder，可能为null
     */
    public static Holder get(Object handler) {
        Method method = null;
        if (handler instanceof HandlerMethod) {
            method = ((HandlerMethod) handler).getMethod();
        } else if (handler instanceof Controller) {
            try {
                method = ((Controller) handler).getClass().getDeclaredMethod("handleRequest", HttpServletRequest.class, HttpServletResponse.class);
            } catch (NoSuchMethodException e) {
                return null;
            }
        } else if (handler instanceof HttpRequestHandler) {
            try {
                method = ((HttpRequestHandler) handler).getClass().getDeclaredMethod("handleRequest", HttpServletRequest.class, HttpServletResponse.class);
            } catch (NoSuchMethodException e) {
                return null;
            }
        } else if (handler instanceof HttpServlet) {
            try {
                method = ((HttpServlet) handler).getClass().getDeclaredMethod("service", HttpServletRequest.class, HttpServletResponse.class);
            } catch (NoSuchMethodException e) {
                return null;
            }
        }

        if (method == null) {
            return null;
        }

        Annotation methodAnnotation = getHolderImplAnnotation(method.getDeclaredAnnotations());
        Annotation classAnnotation = getHolderImplAnnotation(method.getDeclaringClass().getDeclaredAnnotations());
        if (methodAnnotation == null && classAnnotation == null) {
            return null;
        }
        return get(methodAnnotation, classAnnotation);
    }

    public static Holder get(Annotation methodAnnotation, Annotation classAnnotation) {
        HolderImpl holderImpl = null;
        if (methodAnnotation != null) {
            holderImpl = methodAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class);
        }
        if (holderImpl == null && classAnnotation != null) {
            holderImpl = classAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class);
        }
        if (holderImpl == null) {
            return null;
        }
        Class<? extends Annotation> methodAClass = methodAnnotation == null? null: methodAnnotation.annotationType();
        Class<? extends Annotation> classAClass = classAnnotation == null? null: classAnnotation.annotationType();
        if (methodAClass != null && classAClass != null && methodAClass != classAClass) {
            return null;
        }
        final HolderImpl finalHolderImpl = holderImpl;
        return cache.computeIfAbsent(new AnnotationHolder(methodAnnotation, classAnnotation), key -> {
            try {
                Constructor<? extends Holder> constructor = finalHolderImpl.value().getConstructor(methodAClass, methodAClass);
                return constructor.newInstance(methodAnnotation, classAnnotation);
            } catch (NoSuchMethodException e) {
                log.warn("the class of '{}' don't has constructor with both '{}' annotation parameters", finalHolderImpl.value(), methodAnnotation);
                return null;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static Annotation getHolderImplAnnotation(Annotation[] annotations) {
        for (Annotation annotation : annotations) {
            HolderImpl holderImpl = annotation.annotationType().getDeclaredAnnotation(HolderImpl.class);
            if (holderImpl != null) {
                return annotation;
            }
        }
        return null;
    }

    /**
     * @return 算法实现
     */
    @NonNull
    public abstract Arithmetic getArithmetic();

    /**
     * 执行限流
     * @param request 请求
     * @param response 响应
     * @param holder 限流上下文
     * @return true放行，false限流
     */
    public final boolean tryAcquire(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        return getArithmetic().tryAcquire(request, response, holder);
    }

    /**
     * @return 限流时响应状态码
     */
    public abstract int getCode();
    /**
     * @return 限流时响应编码格式
     */
    @NonNull
    public abstract Charset getCharset();
    /**
     * @return 限流时响应媒体类型
     */
    @NonNull
    public abstract MediaType getMediaType();
    /**
     * @return 限流时响应体内容
     */
    public abstract String getBody();

}
