package io.gitee.jtree.starter.ratelimiter.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * 限流全局配置
 */
@Data
@Component
@Validated
@ConfigurationProperties(prefix = "rate.limiter")
public class RateLimitProperties {
    /**
     * 是否显示日志，默认显示
     */
    private boolean showLog = true;
}
