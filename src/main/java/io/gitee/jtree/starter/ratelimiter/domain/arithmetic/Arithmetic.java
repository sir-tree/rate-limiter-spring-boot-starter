package io.gitee.jtree.starter.ratelimiter.domain.arithmetic;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 算法实现基类
 */
public interface Arithmetic {

    Map<Class<? extends Arithmetic>, Arithmetic> cache = new ConcurrentHashMap<>();

    /**
     * 执行限流
     * @param request 请求
     * @param response 响应
     * @param holder 限流上下文
     * @return true放行，false限流
     */
    boolean tryAcquire(HttpServletRequest request, HttpServletResponse response, Holder holder);

    static Arithmetic getInstance(Class<? extends Arithmetic> tClass) {
        return cache.computeIfAbsent(tClass, key -> {
            try {
                return tClass.getConstructor().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

}
