package io.gitee.jtree.starter.ratelimiter.annotation.impl;

import io.gitee.jtree.starter.ratelimiter.annotation.HolderImpl;
import io.gitee.jtree.starter.ratelimiter.config.properties.BucketLimitProperties;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

/**
 * 桶限流算法实现限制，不做任何配置按{@link BucketLimitProperties}的值处理
 */
@Inherited
@Documented
@HolderImpl(BucketLimitHolder.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface BucketLimiter {

    /**
     * @return key存储的池子，用以区分不同池子的Key
     */
    String pool() default "";

    /**
     * @return 值唯一，标识限制上下文
     */
    Class<? extends Key> key() default Key.class;

    /**
     * @return 桶的容量
     */
    long capacity() default -1;

    /**
     * @return 表示间隔duration后填充的令牌数量，小于等于0表示和capacity一样，漏桶算法无效
     */
    long fill() default -1;

    /**
     * @return 表示duration期间放行的数量，小于等于0表示和capacity一样，令牌桶算法无效
     */
    long loopholes() default -1;

    /**
     * @return 令牌桶算法表示间隔时长后对桶填充令牌，漏桶算法时表示间隔时长内允许loopholes个请求
     */
    long duration() default -1;

    /**
     * @return 从黑名单移除时间，小于等于0表示不开启，漏桶算法无效
     */
    long blackDuration() default -1;

    /**
     * @return duration和blackDuration的时间单位
     */
    ChronoUnit timeUnit() default ChronoUnit.FOREVER;

    /**
     * @return 漏桶算法是否公平等待，FALSE 不公平， TRUE公平
     */
    BucketLimitHolder.Fair fair() default BucketLimitHolder.Fair.DEFAULT;

    /**
     * @return 限流算法
     */
    Class<? extends Arithmetic> arithmetic() default Arithmetic.class;

    /**
     * @return 限流生效响应状态码
     */
    int code() default -1;

    /**
     * @return 限流生效响应内容
     */
    String body() default "";

    /**
     * @return 限流生效响应内容媒体类型
     */
    String mediaType() default "";

    /**
     * @return 限流生效响应内容编码
     */
    String charset() default "";

}
