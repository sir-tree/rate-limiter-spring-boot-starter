package io.gitee.jtree.starter.ratelimiter.domain.holder.impl;

import io.gitee.jtree.starter.ratelimiter.annotation.HolderImpl;
import io.gitee.jtree.starter.ratelimiter.annotation.impl.BucketLimiter;
import io.gitee.jtree.starter.ratelimiter.config.properties.BucketLimitProperties;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import io.gitee.jtree.starter.ratelimiter.util.BeanUtils;
import io.gitee.jtree.starter.ratelimiter.util.CommonUtils;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;
import java.time.temporal.ChronoUnit;

public class BucketLimitHolder extends Holder {

    public enum Fair {
        DEFAULT(0),
        TRUE(1),
        FALSE(-1),
        ;
        final int value;
        Fair(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private final String pool;
    private final Key key;
    private final long capacity;
    private final long fill;
    private final long loopholes;
    private final long duration;
    private final Arithmetic arithmetic;
    private final ChronoUnit timeUnit;
    private final long blackDuration;
    private final Fair fair;
    private final int code;
    private final String body;
    private final Charset charset;
    private final MediaType mediaType;

    public BucketLimitHolder(BucketLimiter methodAnnotation, BucketLimiter classAnnotation) {
        super(methodAnnotation == null? null: methodAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class)
                , classAnnotation == null? null: classAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class));

        BucketLimitProperties properties = BeanUtils.bucketLimitProperties;

        this.pool = CommonUtils.get(methodAnnotation != null? methodAnnotation.pool(): ""
                , classAnnotation != null? classAnnotation.pool(): ""
                , properties.getPool());

        this.key = CommonUtils.getKey(methodAnnotation != null? methodAnnotation.key(): null
                , classAnnotation != null? classAnnotation.key() : null
                , properties.getKey());

        this.capacity = CommonUtils.get(methodAnnotation != null? methodAnnotation.capacity(): -1
                , classAnnotation != null? classAnnotation.capacity(): -1
                , properties.getCapacity());

        this.fill = CommonUtils.get(methodAnnotation != null? methodAnnotation.fill(): -1
                , classAnnotation != null? classAnnotation.fill(): -1
                , properties.getFill());

        this.loopholes = CommonUtils.get(methodAnnotation != null? methodAnnotation.loopholes(): -1
                , classAnnotation != null? classAnnotation.loopholes(): -1
                , properties.getLoopholes());

        this.duration = CommonUtils.get(methodAnnotation != null? methodAnnotation.duration(): -1
                , classAnnotation != null? classAnnotation.duration(): -1
                , properties.getDuration());

        this.timeUnit = CommonUtils.get(methodAnnotation != null? methodAnnotation.timeUnit(): ChronoUnit.FOREVER
                , classAnnotation != null? classAnnotation.timeUnit(): ChronoUnit.FOREVER
                , properties.getTimeUnit());

        this.blackDuration = CommonUtils.get(methodAnnotation != null? methodAnnotation.blackDuration(): -1
                , classAnnotation != null? classAnnotation.blackDuration(): -1
                , properties.getBlackDuration());

        this.fair = CommonUtils.get(methodAnnotation != null? methodAnnotation.fair(): null
                , classAnnotation != null? classAnnotation.fair(): null
                , properties.getFair());

        this.arithmetic = CommonUtils.getArithmetic(methodAnnotation != null? methodAnnotation.arithmetic(): null
                , classAnnotation != null? classAnnotation.arithmetic() : null
                , properties.getArithmetic());

        this.code = CommonUtils.get(methodAnnotation != null? methodAnnotation.code(): -1
                , classAnnotation != null? classAnnotation.code(): -1
                , properties.getCode());

        this.body = CommonUtils.get(methodAnnotation != null? methodAnnotation.body(): ""
                , classAnnotation != null? classAnnotation.body(): ""
                , properties.getBody());

        this.charset = Charset.forName(CommonUtils.get(methodAnnotation != null? methodAnnotation.charset(): ""
                , classAnnotation != null? classAnnotation.charset(): ""
                , properties.getCharset()));

        this.mediaType = MediaType.parseMediaType(CommonUtils.get(methodAnnotation != null? methodAnnotation.mediaType(): ""
                , classAnnotation != null? classAnnotation.mediaType(): ""
                , properties.getMediaType()));

    }

    public long getFill() {
        return fill > 0? fill: capacity;
    }

    public String getPool() {
        return pool;
    }

    public Key getKey() {
        return key;
    }

    public long getCapacity() {
        return capacity;
    }

    public long getLoopholes() {
        return loopholes;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public Arithmetic getArithmetic() {
        return arithmetic;
    }

    public ChronoUnit getTimeUnit() {
        return timeUnit;
    }

    public long getBlackDuration() {
        return blackDuration;
    }

    public Fair getFair() {
        return fair;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public Charset getCharset() {
        return charset;
    }

    @Override
    public MediaType getMediaType() {
        return mediaType;
    }
}
