package io.gitee.jtree.starter.ratelimiter.domain.key.impl;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 根据请求路径限制
 */
public class PathKey implements Key {
    @Override
    public String getKey(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        return request.getRequestURI();
    }
}
