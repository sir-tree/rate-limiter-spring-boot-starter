package io.gitee.jtree.starter.ratelimiter.config.validation;

import io.gitee.jtree.starter.ratelimiter.config.validation.impl.NotDefaultTypeImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotDefaultTypeImpl.class)
public @interface NotDefaultType {
    String message() default "该枚举值不被允许";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
