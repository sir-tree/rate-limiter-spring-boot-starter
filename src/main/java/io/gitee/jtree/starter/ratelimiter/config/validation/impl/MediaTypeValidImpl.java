package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.MediaTypeValid;
import org.springframework.http.MediaType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MediaTypeValidImpl implements ConstraintValidator<MediaTypeValid, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            MediaType.parseMediaType(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
