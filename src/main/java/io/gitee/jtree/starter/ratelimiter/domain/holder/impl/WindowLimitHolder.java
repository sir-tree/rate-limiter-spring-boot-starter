package io.gitee.jtree.starter.ratelimiter.domain.holder.impl;

import io.gitee.jtree.starter.ratelimiter.annotation.HolderImpl;
import io.gitee.jtree.starter.ratelimiter.annotation.impl.WindowLimiter;
import io.gitee.jtree.starter.ratelimiter.config.properties.WindowLimitProperties;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import io.gitee.jtree.starter.ratelimiter.util.BeanUtils;
import io.gitee.jtree.starter.ratelimiter.util.CommonUtils;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;
import java.time.temporal.ChronoUnit;

public class WindowLimitHolder extends Holder {

    public enum Type {
        DEFAULT(0),
        FIXED(-1),
        SLIDE(1),
        ;
        final int value;
        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private final String pool;
    private final Key key;
    private final long limit;
    private final long window;
    private final ChronoUnit timeUnit;
    private final Type type;
    private final Arithmetic arithmetic;
    private final int code;
    private final String body;
    private final Charset charset;
    private final MediaType mediaType;

    public WindowLimitHolder(WindowLimiter methodAnnotation, WindowLimiter classAnnotation) {
        super(methodAnnotation == null? null: methodAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class)
                , classAnnotation == null? null: classAnnotation.annotationType().getDeclaredAnnotation(HolderImpl.class));

        WindowLimitProperties properties = BeanUtils.windowLimitProperties;

        this.pool = CommonUtils.get(methodAnnotation != null? methodAnnotation.pool(): ""
                , classAnnotation != null? classAnnotation.pool(): ""
                , properties.getPool());

        this.key = CommonUtils.getKey(methodAnnotation != null? methodAnnotation.key(): null
                , classAnnotation != null? classAnnotation.key() : null
                , properties.getKey());

        this.limit = CommonUtils.get(methodAnnotation != null? methodAnnotation.limit(): -1
                , classAnnotation != null? classAnnotation.limit(): -1
                , properties.getLimit());

        this.window = CommonUtils.get(methodAnnotation != null? methodAnnotation.window(): -1
                , classAnnotation != null? classAnnotation.window(): -1
                , properties.getWindow());

        this.timeUnit = CommonUtils.get(methodAnnotation != null? methodAnnotation.timeUnit(): ChronoUnit.FOREVER
                , classAnnotation != null? classAnnotation.timeUnit(): ChronoUnit.FOREVER
                , properties.getTimeUnit());

        this.type = CommonUtils.get(methodAnnotation != null? methodAnnotation.type(): null
                , classAnnotation != null? classAnnotation.type(): null
                , properties.getType());

        this.arithmetic = CommonUtils.getArithmetic(methodAnnotation != null? methodAnnotation.arithmetic(): null
                , classAnnotation != null? classAnnotation.arithmetic() : null
                , properties.getArithmetic());

        this.code = CommonUtils.get(methodAnnotation != null? methodAnnotation.code(): -1
                , classAnnotation != null? classAnnotation.code(): -1
                , properties.getCode());

        this.body = CommonUtils.get(methodAnnotation != null? methodAnnotation.body(): ""
                , classAnnotation != null? classAnnotation.body(): ""
                , properties.getBody());

        this.charset = Charset.forName(CommonUtils.get(methodAnnotation != null? methodAnnotation.charset(): ""
                , classAnnotation != null? classAnnotation.charset(): ""
                , properties.getCharset()));

        this.mediaType = MediaType.parseMediaType(CommonUtils.get(methodAnnotation != null? methodAnnotation.mediaType(): ""
                , classAnnotation != null? classAnnotation.mediaType(): ""
                , properties.getMediaType()));
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public Charset getCharset() {
        return charset;
    }

    @Override
    public MediaType getMediaType() {
        return mediaType;
    }

    @Override
    public Arithmetic getArithmetic() {
        return arithmetic;
    }

    public String getPool() {
        return pool;
    }

    public Key getKey() {
        return key;
    }

    public long getLimit() {
        return limit;
    }

    public long getWindow() {
        return window;
    }

    public ChronoUnit getTimeUnit() {
        return timeUnit;
    }

    public Type getType() {
        return type;
    }
}
