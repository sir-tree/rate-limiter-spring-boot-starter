package io.gitee.jtree.starter.ratelimiter.util;

import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import org.springframework.util.StringUtils;

import java.time.temporal.ChronoUnit;

public class CommonUtils {

    public static BucketLimitHolder.Fair get(BucketLimitHolder.Fair a, BucketLimitHolder.Fair b, BucketLimitHolder.Fair c) {
        if (a != null && a != BucketLimitHolder.Fair.DEFAULT) {
            return a;
        }
        if (b != null && b != BucketLimitHolder.Fair.DEFAULT) {
            return b;
        }
        return c;
    }

    public static WindowLimitHolder.Type get(WindowLimitHolder.Type a, WindowLimitHolder.Type b, WindowLimitHolder.Type c) {
        if (a != null && a != WindowLimitHolder.Type.DEFAULT) {
            return a;
        }
        if (b != null && b != WindowLimitHolder.Type.DEFAULT) {
            return b;
        }
        return c;
    }

    public static Key getKey(Class<? extends Key> a, Class<? extends Key> b, Class<? extends Key> c) {
        if (a != null && a != Key.class) {
            return Key.getInstance(a);
        }
        if (b != null && b != Key.class) {
            return Key.getInstance(b);
        }
        return Key.getInstance(c);
    }

    public static Arithmetic getArithmetic(Class<? extends Arithmetic> a, Class<? extends Arithmetic> b, Class<? extends Arithmetic> c) {
        if (a != null && a != Arithmetic.class) {
            return Arithmetic.getInstance(a);
        }
        if (b != null && b != Arithmetic.class) {
            return Arithmetic.getInstance(b);
        }
        return Arithmetic.getInstance(c);
    }

    public static String get(String a, String b, String c) {
        if (StringUtils.hasText(a)) {
            return a;
        }
        if (StringUtils.hasText(b)) {
            return b;
        }
        return c;
    }

    public static long get(long a, long b, long c) {
        if (a > 0) {
            return a;
        }
        if (b > 0) {
            return b;
        }
        return c;
    }

    public static int get(int a, int b, int c) {
        if (a > 0) {
            return a;
        }
        if (b > 0) {
            return b;
        }
        return c;
    }

    public static ChronoUnit get(ChronoUnit a, ChronoUnit b, ChronoUnit c) {
        if (a != ChronoUnit.FOREVER) {
            return a;
        }
        if (b != ChronoUnit.FOREVER) {
            return b;
        }
        return c;
    }

}
