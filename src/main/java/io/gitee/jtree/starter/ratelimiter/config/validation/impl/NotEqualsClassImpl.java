package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.NotEqualsClass;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotEqualsClassImpl implements ConstraintValidator<NotEqualsClass, Class<?>> {

    private Class<?> initClass;

    @Override
    public void initialize(NotEqualsClass constraintAnnotation) {
        initClass = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Class<?> aClass, ConstraintValidatorContext constraintValidatorContext) {
        return initClass != aClass;
    }
}
