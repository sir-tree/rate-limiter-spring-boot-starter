package io.gitee.jtree.starter.ratelimiter.config.validation;

import io.gitee.jtree.starter.ratelimiter.config.validation.impl.NotEqualsClassImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEqualsClassImpl.class)
public @interface NotEqualsClass {
    String message() default "不能为此值";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<?> value();
}
