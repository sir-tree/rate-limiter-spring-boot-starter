package io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl;

import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;
import io.gitee.jtree.starter.ratelimiter.util.BeanUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * 滑动窗口/固定窗口算法实现
 */
public class WindowArithmetic implements Arithmetic {

    @Override
    public boolean tryAcquire(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        WindowLimitHolder windowLimitHolder = (WindowLimitHolder) holder;

        String pool = windowLimitHolder.getPool();
        String key = windowLimitHolder.getKey().getKey(request, response, holder);
        String limit = String.valueOf(windowLimitHolder.getLimit());
        String window = String.valueOf(windowLimitHolder.getTimeUnit().getDuration().multipliedBy(windowLimitHolder.getWindow()).toMillis());
        String now = String.valueOf(Instant.now().toEpochMilli());
        WindowLimitHolder.Type type = windowLimitHolder.getType();

        List<String> keys = Collections.singletonList(String.format("%s-%s", pool, key));
        Object[] args = new Object[]{limit, window, now};
        Long result = BeanUtils.redisTemplate
                .execute(type == WindowLimitHolder.Type.SLIDE? BeanUtils.slidingWindowScript: BeanUtils.fixedWindowScript, keys, args);

        return result != null && result == 1;
    }

}
