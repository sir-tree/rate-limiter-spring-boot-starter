package io.gitee.jtree.starter.ratelimiter.domain.key;

import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 限流标识实现基类
 */
public interface Key {

    Map<Class<? extends Key>, Key> cache = new ConcurrentHashMap<>();

    /**
     * @param request 当前请求
     * @param response 当前响应
     * @param holder 当前限流上下文
     * @return Key的值
     */
    String getKey(HttpServletRequest request, HttpServletResponse response, Holder holder);

    static Key getInstance(Class<? extends Key> tClass) {
        return cache.computeIfAbsent(tClass, key -> {
            try {
                return tClass.getConstructor().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}
