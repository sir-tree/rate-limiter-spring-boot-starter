package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.NotDefaultType;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotDefaultTypeImpl implements ConstraintValidator<NotDefaultType, WindowLimitHolder.Type> {

    @Override
    public boolean isValid(WindowLimitHolder.Type value, ConstraintValidatorContext context) {
        return value != WindowLimitHolder.Type.DEFAULT;
    }
}
