package io.gitee.jtree.starter.ratelimiter.annotation;

import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

/**
 * 自定义限流注解必须指定实现{@link io.gitee.jtree.starter.ratelimiter.domain.holder.Holder}类
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface HolderImpl {

    /**
     * @return holder实现类
     */
    @NotNull
    Class<? extends io.gitee.jtree.starter.ratelimiter.domain.holder.Holder> value();
}
