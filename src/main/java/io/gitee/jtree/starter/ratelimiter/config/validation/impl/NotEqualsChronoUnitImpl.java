package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.NotEqualsChronoUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.temporal.ChronoUnit;

public class NotEqualsChronoUnitImpl implements ConstraintValidator<NotEqualsChronoUnit, ChronoUnit> {

    private ChronoUnit initUnit;

    @Override
    public void initialize(NotEqualsChronoUnit constraintAnnotation) {
        initUnit = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(ChronoUnit unit, ConstraintValidatorContext constraintValidatorContext) {
        return initUnit != unit;
    }
}
