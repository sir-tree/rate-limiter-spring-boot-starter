package io.gitee.jtree.starter.ratelimiter.annotation.impl;

import io.gitee.jtree.starter.ratelimiter.annotation.HolderImpl;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;
import io.gitee.jtree.starter.ratelimiter.config.properties.WindowLimitProperties;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

/**
 * 窗口限流，不做任何配置按{@link WindowLimitProperties}的值处理
 */
@Inherited
@Documented
@HolderImpl(WindowLimitHolder.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface WindowLimiter {

    /**
     * @return key存储的池子，用以区分不同池子的Key
     */
    String pool() default "";

    /**
     * @return 值唯一，标识限制上下文
     */
    Class<? extends Key> key() default Key.class;

    /**
     * @return 窗口限流大小
     */
    long limit() default -1;

    /**
     * @return 窗口期时长
     */
    long window() default -1;

    /**
     * @return 时间单位
     */
    ChronoUnit timeUnit() default ChronoUnit.FOREVER;

    /**
     * @return 窗口类型
     */
    WindowLimitHolder.Type type() default WindowLimitHolder.Type.DEFAULT;

    /**
     * @return 算法实现类
     */
    Class<? extends Arithmetic> arithmetic() default Arithmetic.class;

    /**
     * @return 限流生效响应状态码
     */
    int code() default -1;

    /**
     * @return 限流生效响应内容
     */
    String body() default "";

    /**
     * @return 限流生效响应内容媒体类型
     */
    String mediaType() default "";

    /**
     * @return 限流生效响应内容编码
     */
    String charset() default "";
}
