package io.gitee.jtree.starter.ratelimiter.config.validation.impl;

import io.gitee.jtree.starter.ratelimiter.config.validation.NotDefaultFair;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotDefaultFairImpl implements ConstraintValidator<NotDefaultFair, BucketLimitHolder.Fair> {

    @Override
    public boolean isValid(BucketLimitHolder.Fair value, ConstraintValidatorContext context) {
        return value != BucketLimitHolder.Fair.DEFAULT;
    }
}
