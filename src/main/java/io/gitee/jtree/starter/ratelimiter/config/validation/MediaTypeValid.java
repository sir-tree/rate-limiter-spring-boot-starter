package io.gitee.jtree.starter.ratelimiter.config.validation;


import io.gitee.jtree.starter.ratelimiter.config.validation.impl.MediaTypeValidImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MediaTypeValidImpl.class)
public @interface MediaTypeValid {
    String message() default "媒体格式不存在或格式错误";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
