package io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl;

import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.Holder;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.BucketLimitHolder;
import io.gitee.jtree.starter.ratelimiter.util.BeanUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * 令牌桶算法
 */
@Component
public class TokenBucketArithmetic implements Arithmetic {

    @Override
    public boolean tryAcquire(HttpServletRequest request, HttpServletResponse response, Holder holder) {
        BucketLimitHolder bucketLimitHolder = (BucketLimitHolder) holder;

        String pool = bucketLimitHolder.getPool();
        String key = bucketLimitHolder.getKey().getKey(request, response, bucketLimitHolder);

        String capacity = String.valueOf(bucketLimitHolder.getCapacity());
        String fill = String.valueOf(bucketLimitHolder.getFill());
        String now = String.valueOf(Instant.now().toEpochMilli());
        String duration = String.valueOf(bucketLimitHolder.getTimeUnit().getDuration().multipliedBy(bucketLimitHolder.getDuration()).toMillis());
        String blackDuration = "-1";
        if (bucketLimitHolder.getBlackDuration() > 0) {
            blackDuration = String.valueOf(bucketLimitHolder.getTimeUnit().getDuration().multipliedBy(bucketLimitHolder.getBlackDuration()).toMillis());
        }

        List<String> keys = Collections.singletonList(String.format("%s-%s", pool, key));
        Object[] args = new Object[]{capacity, fill, now, duration, blackDuration};
        Long result = BeanUtils.redisTemplate.execute(BeanUtils.tokenBucketScript, keys, args);

        return result != null && result >= 0;
    }
}
