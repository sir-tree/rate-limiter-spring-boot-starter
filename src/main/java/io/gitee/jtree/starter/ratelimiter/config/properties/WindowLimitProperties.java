package io.gitee.jtree.starter.ratelimiter.config.properties;

import io.gitee.jtree.starter.ratelimiter.config.validation.*;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.Arithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.arithmetic.impl.WindowArithmetic;
import io.gitee.jtree.starter.ratelimiter.domain.holder.impl.WindowLimitHolder;
import io.gitee.jtree.starter.ratelimiter.domain.key.Key;
import io.gitee.jtree.starter.ratelimiter.domain.key.impl.IpKey;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;

/**
 * 窗口限流算法全局配置
 */
@Data
@Component
@Validated
@ConfigurationProperties(prefix = "rate.limiter.window")
public class WindowLimitProperties {

    /**
     * Key存储的池子，用以区分不同池子的Key
     */
    @NotBlank
    private String pool = "WindowLimiterPool";

    /**
     * 值唯一，标识限制上下文, 默认值IpKey，即通过Ip进行限制
     */
    @NotEqualsClass(Key.class)
    private Class<? extends Key> key = IpKey.class;

    /**
     * 窗口大小
     */
    @Positive
    private long limit = 30;

    /**
     * 窗口时长
     */
    @Positive
    private long window = 1;

    /**
     * 时间单位
     */
    @NotEqualsChronoUnit(ChronoUnit.FOREVER)
    private ChronoUnit timeUnit = ChronoUnit.SECONDS;

    /**
     * 窗口类型
     */
    @NotDefaultType
    private WindowLimitHolder.Type type = WindowLimitHolder.Type.SLIDE;

    /**
     * 算法实现类
     */
    @NotEqualsClass(Arithmetic.class)
    private Class<? extends Arithmetic> arithmetic = WindowArithmetic.class;


    /**
     * 限流生效响应状态码，默认400
     */
    @Positive
    private int code = 400;

    /**
     * 限流生效响应内容，默认为"Bad Request"
     */
    private String body = "Bad Request";

    /**
     * 限流生效响应内容结构，默认为text/plain
     */
    @MediaTypeValid
    private String mediaType = MediaType.TEXT_PLAIN.toString();

    /**
     * 限流生效响应内容编码，默认为UTF-8
     */
    @CharsetValid
    private String charset = StandardCharsets.UTF_8.name();

}
